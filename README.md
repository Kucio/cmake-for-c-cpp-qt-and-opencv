# cmake for c cpp qt and opencv

My generic cmake setup that I use for several projects.
Opencv and qt libraries needs to be discoverable in LD_LIBRARY_PATH variable
otherwise OPENCV_DIR and QT_DIR variables needs to be defined in CMakeLists.txt
file. This CMake usage requires sources and includes to be put in separate
folders "src" and "include". In case of intention to add cpp and h files in
subfolders then You will need to modify CMakeLists.txt file like:
```
include_directories(
    ${PROJECT_SOURCE_DIR}/include/sub-folder-name
    ${PROJECT_SOURCE_DIR}/src/sub-folder-name
)

file(GLOB all_SRCS
    "${PROJECT_SOURCE_DIR}/include/sub-folder-name/*.h"
    "${PROJECT_SOURCE_DIR}/include/sub-folder-name/*.hpp"
    "${PROJECT_SOURCE_DIR}/src/sub-folder-name/*.cpp"
    "${PROJECT_SOURCE_DIR}/src/sub-folder-name/*.c"
)
```

After succesfull build of application clang-tidy is run to provide additional
correctness check. Example of clang-tidy usage credits:
http://mariobadr.com/using-clang-tidy-with-cmake-36.html